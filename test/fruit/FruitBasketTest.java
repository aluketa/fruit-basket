package fruit;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;

public class FruitBasketTest {

    FruitBasket basket;

    @Before
    public void setUpBasket() {
        Map<String, BigDecimal> prices = new HashMap<>();
        prices.put("Apple", BigDecimal.valueOf(0.35));
        prices.put("Banana", BigDecimal.valueOf(0.20));
        prices.put("Melon", BigDecimal.valueOf(0.50));
        prices.put("Lime", BigDecimal.valueOf(0.15));

        Map<String, Offer> offers = new HashMap<>();
        offers.put("Melon", new Offer(2, 1));
        offers.put("Lime", new Offer(3, 2));

        basket = new FruitBasket(prices, offers);
    }

    @Test
    public void calculatesTheCorrectPriceForOneItem() {
        BigDecimal totalPrice = basket.calculate(singletonList("Apple"));
        assertEquals(totalPrice.doubleValue(), 0.35, 0.01);

    }

    @Test
    public void calculatesTheCorrectPriceForMultipleItems() {
        BigDecimal totalPrice = basket.calculate(asList("Apple", "Apple", "Banana", "Melon", "Lime"));
        assertEquals(totalPrice.doubleValue(), 0.35 + 0.35 + 0.2 + 0.5 + 0.15, 0.01);
    }

    @Test
    public void calculatesZeroPriceForEmptyList() {
        assertEquals(0.0, basket.calculate(emptyList()).doubleValue(), 0.000001);
    }

    @Test
    public void calculatesCorrectPriceForMelons() {
        assertEquals(basket.calculate(singletonList("Melon")).doubleValue(), 0.5, 0.01);
        assertEquals(basket.calculate(asList("Melon", "Melon")).doubleValue(), 0.5, 0.01);
        assertEquals(basket.calculate(asList("Melon", "Melon", "Melon")).doubleValue(), 1.0, 0.01);
        assertEquals(basket.calculate(asList("Melon", "Melon", "Melon", "Melon")).doubleValue(), 1.0, 0.01);
        assertEquals(basket.calculate(asList("Melon", "Melon", "Melon", "Melon", "Melon")).doubleValue(), 1.5, 0.01);
    }

    @Test
    public void calculatesCorrectPriceForLimes() {
        assertEquals(basket.calculate(singletonList("Lime")).doubleValue(), 0.15, 0.01);
        assertEquals(basket.calculate(asList("Lime", "Lime")).doubleValue(), 0.30, 0.01);
        assertEquals(basket.calculate(asList("Lime", "Lime", "Lime")).doubleValue(), 0.30, 0.01);
        assertEquals(basket.calculate(asList("Lime", "Lime", "Lime", "Lime")).doubleValue(), 0.45, 0.01);
        assertEquals(basket.calculate(asList("Lime", "Lime", "Lime", "Lime", "Lime")).doubleValue(), 0.6, 0.01);
        assertEquals(basket.calculate(asList("Lime", "Lime", "Lime", "Lime", "Lime", "Lime")).doubleValue(), 0.6, 0.01);
    }
}
