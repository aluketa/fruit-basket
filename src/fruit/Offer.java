package fruit;

class Offer {
    public static Offer DEFAULT_OFFER = new Offer(1, 1);

    private int x;
    private int y;

    public Offer(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}