package fruit;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import static fruit.Offer.DEFAULT_OFFER;
import static java.math.BigDecimal.ZERO;
import static java.util.stream.Collectors.groupingBy;

public class FruitBasket {

    private Map<String, BigDecimal> prices;
    private Map<String, Offer> offers;

    public FruitBasket(Map<String, BigDecimal> prices, Map<String, Offer> offers) {
        this.prices = prices;
        this.offers = offers;
    }

    public BigDecimal calculate(List<String> items) {
        return
            items.stream()
                .collect(groupingBy(i -> i))
                .entrySet()
                .stream()
                .map(e -> totalPrice(
                        e.getValue().size(),
                        prices.getOrDefault(e.getKey(), ZERO),
                        offers.getOrDefault(e.getKey(), DEFAULT_OFFER)))
                .reduce(ZERO, BigDecimal::add);
    }

    private BigDecimal totalPrice(int count, BigDecimal price, Offer offer) {
        if (count < offer.getX()) {
            return price.multiply(new BigDecimal(count));
        } else {
            return new BigDecimal(count / offer.getX()).multiply(new BigDecimal(offer.getY())).multiply(price)
                    .add(new BigDecimal(count % offer.getX()).multiply(price));
        }
    }
}